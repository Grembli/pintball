﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper_scr : MonoBehaviour {
    int bumper_score = 100;
    public Light Bumper_light;

	// Use this for initialization
	void Start () {
        Bumper_light.enabled = false;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Ball"))
        {
            Score_manager_scr.instance.update_Score(bumper_score);
            Bumper_light.enabled = true;
            StartCoroutine(waitSeconds(0.5f));
        }
    }
    IEnumerator waitSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Bumper_light.enabled = false;
    }
}

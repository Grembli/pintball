﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class close_trigger_src : MonoBehaviour {

    public GameObject to_close_object;
    public Vector3 start_pos = new Vector3(0f, 0f, 0f); //launcher_door (3.5f, -0.25f, 5.9f);
    public Vector3 end_pos = new Vector3(0f, 0f, 0f); //launcher door 3.5f, 0f, 5.9f);

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            to_close_object.transform.position = end_pos;

        }
    }
}

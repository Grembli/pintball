﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball_scr : MonoBehaviour {
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bumper"))
        {
            Vector3 explosionPos = collision.contacts[0].point;
            this.GetComponent<Rigidbody>().AddExplosionForce(20f, explosionPos, 0f);
        }
    }
}

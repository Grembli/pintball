﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score_manager_scr : MonoBehaviour
{
    //score manager vars
    public static Score_manager_scr instance;
    int balls;
    int Total_score;
    //ball init pos
    Vector3 ball_pos;
    //text var
    public Text text;

    //launcher door vars
    GameObject launcher_door;
    //target vars
    GameObject target1;
    GameObject target2;
    GameObject target3;

    bool is_target1_active;
    bool is_target2_active;
    bool is_target3_active;

    //dor var
    Vector3 start_pos;

    //laser_tie var
    public ParticleSystem laser_shot;
    public ParticleSystem laser_shot1;
    public ParticleSystem laser_shot2;
    public ParticleSystem laser_shot3;
    public ParticleSystem laser_shot4;
    public ParticleSystem laser_shot5;

    void Start()
    {
        instance = this;
        DontDestroyOnLoad(this);
        target1 = GameObject.Find("Target1");
        target2 = GameObject.Find("Target2");
        target3 = GameObject.Find("Target3");
        balls = 3;
        Total_score = 0;
        ball_pos = new Vector3(3.75f, 0.1f, -5.15f);
        start_pos = new Vector3(2.77f, -0.25f, 4.205f);

        is_target1_active = false;
        is_target2_active = false;
        is_target3_active = false;

        laser_shot.Stop();
        laser_shot1.Stop();
        laser_shot2.Stop();
        laser_shot3.Stop();
        laser_shot4.Stop();
        laser_shot5.Stop();

    }

    void Update()
    {

        if (balls <= 0)
        {
            //game over scene
        }
        else
        {
            text.text = "Score:" + Total_score + Environment.NewLine + " lives:" + balls;
            
        }
        
        
    }

    public void ball_die(Rigidbody ball) 
    {
        balls = balls -1;
        ball.transform.position = ball_pos;
        reset_launcher_door_pos();
    }

    public void update_Score(int score)
    {
        Total_score = Total_score + score;
        
    }

    public void reset_launcher_door_pos()
    {
        launcher_door = GameObject.Find("Door");
        launcher_door.transform.position = start_pos;
    }


    public void Win_game()
    {

        is_target1_active = target1.gameObject.GetComponent<Targets_src>().getActive();
        is_target2_active = target2.gameObject.GetComponent<Targets_src>().getActive();
        is_target3_active = target3.gameObject.GetComponent<Targets_src>().getActive();
        if(is_target1_active && is_target2_active && is_target3_active)
        {
            //win game ties shoot, xwing down
            Debug.Log("1" + is_target1_active);
            Debug.Log("2" + is_target2_active);
            Debug.Log("3" + is_target3_active);

            laser_shot.Play();
            laser_shot1.Play();
            laser_shot2.Play();
            laser_shot3.Play();
            laser_shot4.Play();
            laser_shot5.Play();

        }
    }
    


}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightsaber_emisive : MonoBehaviour {
    public Material Mat_emisive_off;
    public Material Mat_emisive_on;
    public Light saber_light;
    int count_time;
    bool time;
    // Use this for initialization
    void Start () {
        saber_light.enabled = false;
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {

            GetComponent<Renderer>().material = Mat_emisive_on;
            saber_light.enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {

            
            StartCoroutine(waitSeconds(1));



        }
    }

    IEnumerator waitSeconds(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        saber_light.enabled = false;
        GetComponent<Renderer>().material = Mat_emisive_off;
    }
}

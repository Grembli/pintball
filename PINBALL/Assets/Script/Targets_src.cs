﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targets_src : MonoBehaviour {

    public Light Active_Light;
    public bool isActive;
    // Use this for initialization
    void Start () {
        Active_Light.enabled = false;
        isActive = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            
            if (!Active_Light.enabled)
            {
                Active_Light.enabled = true;
                isActive = true;
            }
            else if (Active_Light.enabled)
            {
                Active_Light.enabled = false;
                isActive = false;
            }
            Score_manager_scr.instance.Win_game();
        }
    }
    public bool getActive()
    {
        return isActive;
    }
}
